
document.getElementById("dashBoardHeaderIcon").addEventListener("mouseover",mouseOver);
document.getElementById("dashBoardHeaderIcon").addEventListener("mouseout",mouseOut);

document.getElementById("notificationHeaderIcon").addEventListener("mouseover",mouseOver);
document.getElementById("notificationHeaderIcon").addEventListener("mouseout",mouseOut);

document.getElementById("saveHeaderIcon").addEventListener("mouseover",mouseOver);
document.getElementById("saveHeaderIcon").addEventListener("mouseout",mouseOut);

document.getElementById("addOneHeaderIcon").addEventListener("mouseover",mouseOver);
document.getElementById("addOneHeaderIcon").addEventListener("mouseout",mouseOut);

document.getElementById("submitButton").addEventListener('click',submitClicked);



setInterval(function() {
    var rows=document.getElementById('actualTable').rows.length;
    document.getElementById("totalUserHolder").innerHTML=rows;
},2000);



function mouseOver(event) {
  
    switch(event.target.id){
        case 'dashBoardHeaderIcon':
            document.getElementById("dashBoardHeaderIcon").src="images/ic_dashboard_white_24dp_2x.png";
            break;
        case 'notificationHeaderIcon':
            document.getElementById("notificationHeaderIcon").src="images/ic_notifications_white_24dp_2x.png";
            break;
            
        case 'saveHeaderIcon':
            document.getElementById("saveHeaderIcon").src="images/ic_save_white_24dp_2x.png";
            break; 
            
        case 'addOneHeaderIcon':
            document.getElementById("addOneHeaderIcon").src="images/ic_plus_one_white_24dp_2x.png";
            break;
    }
    
    
}

function mouseOut(event) {
    switch(event.target.id){
        case 'dashBoardHeaderIcon':
            document.getElementById("dashBoardHeaderIcon").src="images/ic_dashboard_black_24dp_2x.png";
            break;
        case 'notificationHeaderIcon':
            document.getElementById("notificationHeaderIcon").src="images/ic_notifications_black_24dp_2x.png";
            break;
            
        case 'saveHeaderIcon':
            document.getElementById("saveHeaderIcon").src="images/ic_save_black_24dp_2x.png";
            break; 
            
        case 'addOneHeaderIcon':
            document.getElementById("addOneHeaderIcon").src="images/ic_exposure_plus_1_black_24dp_2x.png";
            break;
    }
}


function submitClicked(event){

    document.getElementById('inputs').value='';
    
}


